package com.google.developer.bugmaster;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.google.developer.bugmaster.data.Insect;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.google.developer.bugmaster.InsectDetailsActivity.EXTRA_SELECTED_INSECT;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class InsectDetailsActivityTest {
    private static final String EXPECTED_INSECT_NAME = "some name";
    private static final String EXPECTED_INSECT_SCIENTIFIC_NAME = "some scientific name";
    private static final String EXPECTED_INSECT_CLASSIFICATION = "some classification";
    @Rule
    public ActivityTestRule<InsectDetailsActivity> mActivityRule =
            new ActivityTestRule<InsectDetailsActivity>(InsectDetailsActivity.class) {
                @Override
                protected Intent getActivityIntent() {
                    Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
                    Intent result = new Intent(targetContext, InsectDetailsActivity.class);
                    result.putExtra(EXTRA_SELECTED_INSECT, new Insect(EXPECTED_INSECT_NAME, EXPECTED_INSECT_SCIENTIFIC_NAME, EXPECTED_INSECT_CLASSIFICATION, "file", 5));
                    return result;
                }
            };

    @Test
    public void verifyShownDetails() {
        onView(allOf(withId(R.id.name), isDisplayed())).check(matches(withText(EXPECTED_INSECT_NAME)));
        onView(allOf(withId(R.id.scientificName), isDisplayed())).check(matches(withText(EXPECTED_INSECT_SCIENTIFIC_NAME)));
        onView(allOf(withId(R.id.classification), isDisplayed())).check(matches(withText(
                InstrumentationRegistry.getInstrumentation().getTargetContext().getString(R.string.classification, EXPECTED_INSECT_CLASSIFICATION))));
    }
}