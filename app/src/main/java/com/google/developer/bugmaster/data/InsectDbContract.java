package com.google.developer.bugmaster.data;

import android.provider.BaseColumns;

final class InsectDbContract {
    private InsectDbContract() {
    }

    static class InsectEntry implements BaseColumns {
        static final String TABLE_NAME = "bugs";
        static final String COLUMN_NAME_FRIENDLY_NAME = "friendlyName";
        static final String COLUMN_NAME_SCIENTIFIC_NAME = "scientificName";
        static final String COLUMN_NAME_CLASSIFICATION = "classification";
        static final String COLUMN_NAME_IMAGE_ASSET = "imageAsset";
        static final String COLUMN_NAME_DANGER_LEVEL = "dangerLevel";
    }
}