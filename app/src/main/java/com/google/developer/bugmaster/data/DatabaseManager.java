package com.google.developer.bugmaster.data;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;

/**
 * Singleton that controls access to the SQLiteDatabase instance
 * for this application.
 */
public class DatabaseManager {
    public static final String SORT_INSECTS_BY_COMMON_NAME = InsectDbContract.InsectEntry.COLUMN_NAME_FRIENDLY_NAME + " ASC";
    public static final String SORT_INSECTS_BY_DANGEROUS_LEVEL = InsectDbContract.InsectEntry.COLUMN_NAME_DANGER_LEVEL + " DESC";
    private static DatabaseManager sInstance;

    public static synchronized DatabaseManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseManager(context.getApplicationContext());
        }

        return sInstance;
    }

    private BugsDbHelper mBugsDbHelper;

    private DatabaseManager(Context context) {
        mBugsDbHelper = new BugsDbHelper(context);
    }

    /**
     * Return a {@link Cursor} that contains every insect in the database.
     *
     * @param sortOrder Optional sort order string for the query, can be null
     * @return {@link Cursor} containing all insect results.
     */
    public Cursor queryAllInsects(String sortOrder) {
        return mBugsDbHelper.getReadableDatabase().query(InsectDbContract.InsectEntry.TABLE_NAME, null, null, null, null, null, sortOrder);
    }

    /**
     * Return a {@link Cursor} that contains a single insect for the given unique id.
     *
     * @param id Unique identifier for the insect record.
     * @return {@link Cursor} containing the insect result.
     */
    public Cursor queryInsectsById(int id) {
        return mBugsDbHelper.getReadableDatabase().query(InsectDbContract.InsectEntry.TABLE_NAME, null, InsectDbContract.InsectEntry._ID + "=?", new String[]{Integer.toString(id)}, null, null, null, null);
    }

    public long queryNumberOfInsects() {
        return DatabaseUtils.queryNumEntries(mBugsDbHelper.getReadableDatabase(), InsectDbContract.InsectEntry.TABLE_NAME);
    }

    public void close() {
        mBugsDbHelper.close();
    }
}
