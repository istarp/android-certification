package com.google.developer.bugmaster.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Parcel;
import android.util.Log;

import com.google.developer.bugmaster.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Database helper class to facilitate creating and updating
 * the database from the chosen schema.
 */
class BugsDbHelper extends SQLiteOpenHelper {
    private static final String TAG = BugsDbHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "insects.db";
    private static final int DATABASE_VERSION = 1;
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + InsectDbContract.InsectEntry.TABLE_NAME + " (" +
                    InsectDbContract.InsectEntry._ID + " INTEGER PRIMARY KEY," +
                    InsectDbContract.InsectEntry.COLUMN_NAME_FRIENDLY_NAME + " TEXT," +
                    InsectDbContract.InsectEntry.COLUMN_NAME_SCIENTIFIC_NAME + " TEXT," +
                    InsectDbContract.InsectEntry.COLUMN_NAME_CLASSIFICATION + " TEXT," +
                    InsectDbContract.InsectEntry.COLUMN_NAME_IMAGE_ASSET + " TEXT," +
                    InsectDbContract.InsectEntry.COLUMN_NAME_DANGER_LEVEL + " INTEGER)";
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + InsectDbContract.InsectEntry.TABLE_NAME;

    //Used to read data from res/ and assets/
    private Resources mResources;

    BugsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mResources = context.getResources();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
        try {
            readInsectsFromResources(db);
        } catch (IOException | JSONException e) {
            Log.e(TAG, "Could not store data into database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /**
     * Streams the JSON data from insect.json, parses it, and inserts it into the
     * provided {@link SQLiteDatabase}.
     *
     * @param db Database where objects should be inserted.
     * @throws IOException
     * @throws JSONException
     */
    private void readInsectsFromResources(SQLiteDatabase db) throws IOException, JSONException {
        StringBuilder builder = new StringBuilder();
        InputStream in = mResources.openRawResource(R.raw.insects);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        //Parse resource into key/values
        JSONArray jsonArray = new JSONObject(builder.toString()).getJSONArray("insects");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Map<String, String> map = new HashMap<>();
            Iterator<?> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = (String) keys.next();
                String value = jsonObject.getString(key);
                map.put(key, value);
            }

            Parcel parcel = Parcel.obtain();
            parcel.writeMap(map);
            parcel.setDataPosition(0);
            ContentValues contentValues = ContentValues.CREATOR.createFromParcel(parcel);
            db.insert(InsectDbContract.InsectEntry.TABLE_NAME, null, contentValues);
        }
    }
}
