package com.google.developer.bugmaster.views;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;
import android.widget.TextView;

import com.google.developer.bugmaster.R;

public class DangerLevelView extends TextView {

    public DangerLevelView(Context context) {
        super(context);
    }

    public DangerLevelView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DangerLevelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DangerLevelView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setDangerLevel(int dangerLevel) {
        setText(String.valueOf(dangerLevel));
        DrawableCompat.setTint(getBackground(), getDangerColor(dangerLevel));
    }

    private int getDangerColor(int dangerLevel) {
        String[] colors = getResources().getStringArray(R.array.dangerColors);
        return Color.parseColor(colors[dangerLevel - 1]);
    }

    public int getDangerLevel() {
        return Integer.parseInt(getText().toString());
    }
}
