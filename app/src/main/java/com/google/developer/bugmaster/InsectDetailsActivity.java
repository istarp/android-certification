package com.google.developer.bugmaster;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.developer.bugmaster.data.Insect;

import java.io.IOException;
import java.io.InputStream;

public class InsectDetailsActivity extends AppCompatActivity {
    public static final String EXTRA_SELECTED_INSECT = "selectedInsectKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Insect insect = getIntent().getParcelableExtra(EXTRA_SELECTED_INSECT);
        findViewById(R.id.image).setBackground(getDrawableFromAssets(insect.imageAsset));
        ((TextView) findViewById(R.id.name)).setText(insect.name);
        ((TextView) findViewById(R.id.scientificName)).setText(insect.scientificName);
        ((TextView) findViewById(R.id.classification)).setText(getString(R.string.classification, insect.classification));
        ((RatingBar) findViewById(R.id.dangerRating)).setProgress(insect.dangerLevel);
    }

    private Drawable getDrawableFromAssets(String name) {
        try {
            InputStream inputStream = getAssets().open(name);
            return Drawable.createFromStream(inputStream, null);
        } catch (IOException ex) {
            return null;
        }
    }
}