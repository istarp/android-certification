package com.google.developer.bugmaster;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.developer.bugmaster.data.DatabaseManager;
import com.google.developer.bugmaster.data.Insect;
import com.google.developer.bugmaster.data.InsectRecyclerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.google.developer.bugmaster.QuizActivity.ANSWER_COUNT;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, InsectRecyclerAdapter.OnItemClickListener {
    private static final String EXTRA_SORT_KEY = "sortKey";
    private static final int INSECT_LOADER_ID = 0;
    private String mSortMode;
    private LoaderManager.LoaderCallbacks<Cursor> mInsectCursorLoaderCallbacks = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new InsectAsyncTaskLoader(MainActivity.this, mDatabaseManager, args.getString(EXTRA_SORT_KEY));
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            mAdapter.setData(data);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mAdapter.setData(null);
        }
    };
    private DatabaseManager mDatabaseManager;
    private InsectRecyclerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        mDatabaseManager = DatabaseManager.getInstance(this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new InsectRecyclerAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);

        if (savedInstanceState != null) {
            mSortMode = savedInstanceState.getString(EXTRA_SORT_KEY, DatabaseManager.SORT_INSECTS_BY_COMMON_NAME);
        } else {
            mSortMode = DatabaseManager.SORT_INSECTS_BY_COMMON_NAME;
        }
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_SORT_KEY, mSortMode);
        getSupportLoaderManager().initLoader(INSECT_LOADER_ID, bundle, mInsectCursorLoaderCallbacks);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(EXTRA_SORT_KEY, mSortMode);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        mDatabaseManager.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                if (mSortMode.equals(DatabaseManager.SORT_INSECTS_BY_COMMON_NAME)) {
                    mSortMode = DatabaseManager.SORT_INSECTS_BY_DANGEROUS_LEVEL;
                } else {
                    mSortMode = DatabaseManager.SORT_INSECTS_BY_COMMON_NAME;
                }
                Bundle bundle = new Bundle();
                bundle.putString(EXTRA_SORT_KEY, mSortMode);
                getSupportLoaderManager().restartLoader(INSECT_LOADER_ID, bundle, mInsectCursorLoaderCallbacks);
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* Click events in Floating Action Button */
    @Override
    public void onClick(View v) {
        int correctAnswerIndex = new Random().nextInt(mAdapter.getItemCount() - 1);
        Insect answerInsect = mAdapter.getItem(correctAnswerIndex);
        Map<Integer, Insect> options = new HashMap<>();
        options.put(correctAnswerIndex, answerInsect);

        while (options.size() != ANSWER_COUNT) {
            int generatedIndex = new Random().nextInt(mAdapter.getItemCount() - 1);
            options.put(generatedIndex, mAdapter.getItem(generatedIndex));
        }

        Intent intent = new Intent(this, QuizActivity.class);
        intent.putExtra(QuizActivity.EXTRA_ANSWER, answerInsect);
        intent.putParcelableArrayListExtra(QuizActivity.EXTRA_INSECTS, new ArrayList<Parcelable>(options.values()));
        startActivity(intent);
    }

    @Override
    public void onInsectClicked(int position) {
        Intent intent = new Intent(this, InsectDetailsActivity.class);
        intent.putExtra(InsectDetailsActivity.EXTRA_SELECTED_INSECT, mAdapter.getItem(position));
        startActivity(intent);
    }
}
