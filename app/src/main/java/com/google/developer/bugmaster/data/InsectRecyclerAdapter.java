package com.google.developer.bugmaster.data;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.developer.bugmaster.R;
import com.google.developer.bugmaster.views.DangerLevelView;

/**
 * RecyclerView adapter extended with project-specific required methods.
 */

public class InsectRecyclerAdapter extends RecyclerView.Adapter<InsectRecyclerAdapter.InsectHolder> {
    public interface OnItemClickListener {
        void onInsectClicked(int position);
    }

    /* ViewHolder for each insect item */
    static class InsectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final DangerLevelView dangerLevel;
        private final TextView commonName;
        private final TextView scientificName;
        private final OnItemClickListener mOnItemClickListener;

        InsectHolder(View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            dangerLevel = (DangerLevelView) itemView.findViewById(R.id.danger_level);
            commonName = (TextView) itemView.findViewById(R.id.name);
            scientificName = (TextView) itemView.findViewById(R.id.scientificName);
            mOnItemClickListener = onItemClickListener;
        }

        void bind(Insect insect) {
            dangerLevel.setDangerLevel(insect.dangerLevel);
            commonName.setText(insect.name);
            scientificName.setText(insect.scientificName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClickListener.onInsectClicked(getAdapterPosition());
        }
    }

    private Cursor mCursor;
    private final OnItemClickListener mOnItemClickListener;

    public InsectRecyclerAdapter(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public void setData(Cursor cursor) {
        mCursor = cursor;
        notifyDataSetChanged();
    }

    @Override
    public InsectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_insect, parent, false);
        return new InsectHolder(itemView, mOnItemClickListener);
    }

    @Override
    public void onBindViewHolder(InsectHolder holder, int position) {
        holder.bind(getItem(position));
    }

    @Override
    public int getItemCount() {
        return mCursor == null ? 0 : mCursor.getCount();
    }

    /**
     * Return the {@link Insect} represented by this item in the adapter.
     *
     * @param position Adapter item position.
     * @return A new {@link Insect} filled with this position's attributes
     * @throws IllegalArgumentException if position is out of the adapter's bounds.
     */
    public Insect getItem(int position) {
        if (position < 0 || position >= getItemCount()) {
            throw new IllegalArgumentException("Item position is out of adapter's range");
        } else if (mCursor.moveToPosition(position)) {
            return new Insect(mCursor);
        }
        return null;
    }
}
