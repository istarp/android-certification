package com.google.developer.bugmaster.reminders;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.developer.bugmaster.MainActivity;
import com.google.developer.bugmaster.QuizActivity;
import com.google.developer.bugmaster.R;
import com.google.developer.bugmaster.data.DatabaseManager;
import com.google.developer.bugmaster.data.Insect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.google.developer.bugmaster.QuizActivity.ANSWER_COUNT;

public class ReminderService extends IntentService {

    private static final String TAG = ReminderService.class.getSimpleName();

    private static final int NOTIFICATION_ID = 42;

    public ReminderService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "Quiz reminder event triggered");

        //Present a notification to the user
        NotificationManager manager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        DatabaseManager databaseManager = DatabaseManager.getInstance(this);
        int numberOfEntries = (int) databaseManager.queryNumberOfInsects();

        int correctAnswerId = new Random().nextInt(numberOfEntries - 1) + 1;
        Cursor correctCursor = databaseManager.queryInsectsById(correctAnswerId);
        correctCursor.moveToFirst();
        Insect answerInsect = new Insect(correctCursor);
        correctCursor.close();
        Map<Integer, Insect> options = new HashMap<>();
        options.put(correctAnswerId, answerInsect);

        while (options.size() != ANSWER_COUNT) {
            int generatedId = new Random().nextInt(numberOfEntries - 1) + 1;
            Cursor cursor = databaseManager.queryInsectsById(generatedId);
            cursor.moveToFirst();
            options.put(generatedId, new Insect(cursor));
            cursor.close();
        }

        databaseManager.close();

        Intent backIntent = new Intent(this, MainActivity.class);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        //Create action intent
        Intent action = new Intent(this, QuizActivity.class);
        action.putExtra(QuizActivity.EXTRA_ANSWER, answerInsect);
        action.putParcelableArrayListExtra(QuizActivity.EXTRA_INSECTS, new ArrayList<Parcelable>(options.values()));

        PendingIntent operation =
                PendingIntent.getActivities(this, 0, new Intent[]{backIntent, action}, PendingIntent.FLAG_CANCEL_CURRENT);

        Notification note = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_text))
                .setSmallIcon(R.drawable.ic_bug_empty)
                .setContentIntent(operation)
                .setAutoCancel(true)
                .build();

        manager.notify(NOTIFICATION_ID, note);

        AlarmReceiver.scheduleAlarm(this);
    }
}
