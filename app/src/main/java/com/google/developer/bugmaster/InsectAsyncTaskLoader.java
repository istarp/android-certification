package com.google.developer.bugmaster;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

import com.google.developer.bugmaster.data.DatabaseManager;

class InsectAsyncTaskLoader extends AsyncTaskLoader<Cursor> {
    private Cursor mCursor;
    private final DatabaseManager mDatabaseManager;
    private final String mSortType;

    InsectAsyncTaskLoader(Context context, DatabaseManager databaseManager, String sortType) {
        super(context);
        mDatabaseManager = databaseManager;
        mSortType = sortType;
    }

    @Override
    protected void onStartLoading() {
        if (mCursor != null) {
            deliverResult(mCursor);
        } else {
            forceLoad();
        }
    }

    @Override
    public Cursor loadInBackground() {
        return mDatabaseManager.queryAllInsects(mSortType);
    }

    @Override
    public void deliverResult(Cursor data) {
        mCursor = data;
        super.deliverResult(data);
    }
}